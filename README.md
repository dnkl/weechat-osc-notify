# osc-notify

osc-notify is a Python script for [WeeChat](https://weechat.org/),
providing notifications using OSC-777 terminal escape sequences.

It is a modified version of
https://artisan.karma-lab.net/ajouter-notification-a-urxvt (no longer
available), by ulhume.

Supported terminals (let me know if there are others!):

* [foot](https://codeberg.org/dnkl/foot)
* [urxvt](http://software.schmorp.de/pkg/rxvt-unicode.html) (needs
  [configuration](https://phyks.me/2014/02/local-notifications-for-weechat-and-urxvt.html))

## Installation

1. Make sure you have the Python plugin enabled (run `/plugin list` in
   WeeChat)
2. Copy to `~/.weechat/python/autoload/osc_notify.py`
