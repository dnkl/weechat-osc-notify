# Original script by ulhume
# https://artisan.karma-lab.net/ajouter-notification-a-urxvt

# Changes
#
# * Write directly to TTY instead of spawning a shell
# * Terminate OSC with ST instead of BEL
# * Code cleanup

from typing import List

import weechat


weechat.register(
    'osc_notify', 'ulhume', '0.1.2', 'GPL3',
    'Terminal OSC notification', '', '')


weechat.hook_print('', 'irc_privmsg', '', 1, 'on_privmsg', '')


def notify(title, message):
    with open('/dev/tty', 'w') as tty:
        tty.write(f'\033]777;notify;{title};{message}\033\\')


def on_privmsg(data, buffer, date, tags: List[str], displayed: int,
               highlight: int, prefix: str, message: str):

    tags = set(tags.split(','))
    displayed = bool(displayed)
    highlight = bool(highlight)

    # bufname = weechat.buffer_get_string(buffer, 'name')
    bufshortname = weechat.buffer_get_string(buffer, 'short_name')

    is_public = 'notify_message' in tags
    is_private = 'notify_private' in tags

    # print(f'tags: {tags}, buffer-name: {bufshortname}, displayed: {displayed}, '
    #       f'highlight: {highlight}, is_public: {is_public}, '
    #       f'is_private: {is_private}, message: {message}')

    if (is_public and highlight):
        notify(f'{prefix}@{bufshortname}', message)

    elif is_private:
        notify(prefix, message)

    return weechat.WEECHAT_RC_OK
